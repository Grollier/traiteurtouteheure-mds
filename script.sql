CREATE TABLE Produit (
  idProduit UUID PRIMARY KEY,
  nom VARCHAR(255) NOT NULL,
  description TEXT,
  uniteMesure VARCHAR(255) NOT NULL,
  categorie VARCHAR(255) NOT NULL
);

CREATE TABLE "Order" (
  idCommande INT PRIMARY KEY,
  dateCommande DATE NOT NULL,
  recetteCommande INT NOT NULL,
  FOREIGN KEY (recetteCommande) REFERENCES Recipe(idRecipe)
);

CREATE TABLE Recipe (
  idRecipe INT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  image TEXT NOT NULL
);

CREATE TABLE Ingredient (
  idIngredient INT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  consistency VARCHAR(255) NOT NULL,
  quantity DOUBLE PRECISION NOT NULL
);

CREATE TABLE RecipeDetail (
  idRecipe INT PRIMARY KEY,
  resume TEXT NOT NULL,
  preparationTime INT NOT NULL,
  cookTime INT NOT NULL,
  ingredients TEXT NOT NULL,
  instructions TEXT NOT NULL,
  FOREIGN KEY (idRecipe) REFERENCES Recipe(idRecipe)
);

CREATE TABLE Stock (
  idStock UUID PRIMARY KEY,
  idProduit UUID NOT NULL,
  prix FLOAT NOT NULL,
  quantite INT NOT NULL,
  FOREIGN KEY (idProduit) REFERENCES Produit(idProduit)
);
