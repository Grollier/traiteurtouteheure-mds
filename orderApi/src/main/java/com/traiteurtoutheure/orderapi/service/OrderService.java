package com.traiteurtoutheure.orderapi.service;

import com.traiteurtoutheure.orderapi.model.Recipe;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    public String getOrder(int idOrder){
        Recipe recipe = new Recipe("Cassoulet", "img/cassoulet", 1);
        return String.valueOf(recipe);
    }
}
