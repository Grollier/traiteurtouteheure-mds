package com.traiteurtoutheure.orderapi.controller;

import com.traiteurtoutheure.orderapi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/recipe/all")
    public String getAllRecipe(@RequestParam int idOrder) {

        try {
            return orderService.getOrder(1);
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Erreur");
        }
    }
}
