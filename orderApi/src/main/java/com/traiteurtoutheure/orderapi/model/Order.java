package com.traiteurtoutheure.orderapi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.Date;

@Data
@Table(name="Order")

public class Order {


    @Id
    @Column(name = "idCommande")
    private int idCommande;

    @Column(name = "dateCommande")
    private Date dateCommande;

    @Column(name = "recetteCommande")
    private Recipe recetteCommande;

}
