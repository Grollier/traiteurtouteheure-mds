package com.mds.recipeapi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@Table(name="RecipeDetail")
public class RecipeDetail {

    @Id
    @Column(name = "idRecipe")
    private int idRecipe;

    @Column(name = "resume")
    private String resume;

    @Column(name = "preparationTime")
    private Integer preparationTime;

    @Column(name = "cookTime")
    private Integer cookTime;

    @Column(name = "ingredients")
    private List<Ingredient> ingredients;

    @Column(name = "instructions")
    private String instructions;

    public RecipeDetail(int idRecipe, String resume, Integer preparationTime, Integer cookTime, List<Ingredient> ingredients, String instructions) {
        this.idRecipe = idRecipe;
        this.resume = resume;
        this.preparationTime = preparationTime;
        this.cookTime = cookTime;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }
}
