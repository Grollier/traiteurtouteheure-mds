package com.mds.recipeapi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.UUID;

@Data
@Entity
@Table(name="Recipe")
public class Recipe {

    @Id
    @Column(name = "idRecipe")
    private int idRecipe;

    @Column(name = "name")
    private String name;

    @Column(name = "image")
    private String image;

    public Recipe(String name, String image, int idRecipe) {
        this.name = name;
        this.image = image;
        this.idRecipe = idRecipe;
    }

    public Recipe() {

    }
}
