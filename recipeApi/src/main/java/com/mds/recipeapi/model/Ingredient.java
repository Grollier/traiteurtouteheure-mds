package com.mds.recipeapi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.UUID;

@Data
@Entity
@Table(name="Ingredient")
public class Ingredient {

    @Id
    @Column(name = "idIngredient")
    private int idIngredient;

    @Column(name = "name")
    private String name;

    @Column(name = "consistency")
    private String consistency;

    @Column(name = "quantity")
    private Double quantity;

}
