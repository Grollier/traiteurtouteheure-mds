package com.mds.recipeapi.service;

import com.mds.recipeapi.model.Ingredient;
import com.mds.recipeapi.model.Recipe;
import com.mds.recipeapi.model.RecipeDetail;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RecipeService {

    Recipe[] recipes = new Recipe[3];

    RecipeDetail recipeDetail;

    public String getAll(){
        this.recipes[0] = new Recipe("Cassoulet", "img/cassoulet", 1);
        this.recipes[1] = new Recipe("Blanquette de Veau", "img/blanquetteVeau", 2);
        this.recipes[2] = new Recipe("Tartes aux Pommes", "img/tartesPommes", 3);

        return Arrays.toString(recipes);
    }

    public String getRecipeDetail(){
        List<Ingredient> ingredients = new ArrayList<>();
        this.recipeDetail = new RecipeDetail(1,"Un délicieux cassoulet toulousain", 30, 180, ingredients, "Découpez la viande...");

        Ingredient ingredient1 = new Ingredient();
        ingredient1.setIdIngredient(34);
        ingredient1.setConsistency("Solide");
        ingredient1.setName("Haricot");
        ingredient1.setQuantity(300.0);
        ingredients.add(ingredient1);

        Ingredient ingredient2 = new Ingredient();
        ingredient2.setIdIngredient(65);
        ingredient2.setConsistency("Solide");
        ingredient2.setName("Saucisse de Toulouse");
        ingredient2.setQuantity(600.0);
        ingredients.add(ingredient2);

        return String.valueOf(recipeDetail);
    }
}
