package com.mds.recipeapi.controller;

import com.mds.recipeapi.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

@Controller
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @GetMapping("/recipe/all")
    public String getAllRecipe() {

        try {
            return recipeService.getAll();
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Erreur");
        }
    }

    @GetMapping("/recipe")
    public String getRecipeDetail(@RequestParam int idRecipe) {

        try {
            return recipeService.getRecipeDetail();
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Erreur");
        }
    }
}
