package com.mds.recipeapi;

import com.mds.recipeapi.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.io.IOException;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class RecipeApiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(RecipeApiApplication.class, args);
	}

	@Autowired
	RecipeService recipeService;

	@Override
	public void run(String... args) throws IOException {
		System.out.println("Hello World");
		System.out.println(this.recipeService.getAll());
		System.out.println(this.recipeService.getRecipeDetail());
	}
}
