package com.stockapi.service;

import com.stockapi.model.Produit;
import com.stockapi.repository.ProduitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceProduit {

    @Autowired
    private ProduitRepository repository;

    public List<Produit> getProduits () {
        return repository.getProduits();
    }

    public void addProduit(Produit produit) {
        repository.addProduit(produit.getNom(), produit.getDescription(), produit.getUniteMesure(), produit.getCategorie());
    }
}