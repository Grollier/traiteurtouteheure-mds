package com.stockapi.service;

import com.stockapi.model.Stock;
import com.stockapi.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceStock {

    @Autowired
    private StockRepository repository;

    public List<Stock> getStocks () {
        return repository.getStocks();
    }

    public void modifyStocks(Stock stock) {
    }
}