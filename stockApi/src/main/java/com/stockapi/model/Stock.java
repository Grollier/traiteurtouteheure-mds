package com.stockapi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Data
@Entity
@Getter
@Setter
@Table(name = "Stock")
public class Stock {

    @Id
    @Column(name = "id_stock")
    private UUID idStock;

    @Id
    @Column(name = "id_produit")
    private UUID idProduit;

    @Column(name = "prix")
    private float prix;

    @Column(name = "quantite")
    private int quantite;

}