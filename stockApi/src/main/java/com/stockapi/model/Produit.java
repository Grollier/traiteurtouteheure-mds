package com.stockapi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Data
@Entity
@Getter
@Setter
@Table(name = "Produit")
public class Produit {

    @Id
    @Column(name = "id_produit")
    private UUID idProduit;

    @Column(name = "nom")
    private String nom;

    @Column(name = "description")
    private String description;

    @Column(name = "unite_mesure")
    private String uniteMesure;

    @Column(name = "categorie")
    private String categorie;
}