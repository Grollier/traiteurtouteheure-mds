package com.stockapi.controller;

import com.stockapi.model.Stock;
import com.stockapi.service.ServiceStock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StockController {

    @Autowired
    private ServiceStock serviceStock;

    @GetMapping("/stocks")
    public List<Stock> getStocks() {
        return serviceStock.getStocks();
    }


}