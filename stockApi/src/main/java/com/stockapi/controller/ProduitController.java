package com.stockapi.controller;

import com.stockapi.model.Produit;

import com.stockapi.repository.ProduitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.stockapi.service.ServiceProduit;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProduitController {

    @Autowired
    private ServiceProduit serviceProduit;

    @GetMapping("/produits")
    public List<Produit> getProduits() {
        return serviceProduit.getProduits();
    }

    @GetMapping("/produits/ajouter")
    public void ajouterProduit(@RequestBody Produit produit) {
        serviceProduit.addProduit(produit);
    }
}