package com.stockapi.repository;

import com.stockapi.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProduitRepository extends JpaRepository<Produit, Long> {

    @Query("SELECT '*' FROM Produit")
    List<Produit> getProduits();

    @Modifying
    @Query(value = "INSERT INTO Produit VALUES (:nom,:desc,:unite,:categorie)", nativeQuery = true)
    @Transactional
    void addProduit(@Param("nom") String nom, @Param("desc") String description,
                    @Param("unite") String unite, @Param("categorie") String categorie);
}