package com.stockapi.repository;

import com.stockapi.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface StockRepository extends JpaRepository<Stock, Long> {
    @Query("SELECT '*' FROM Stock")
    List<Stock> getStocks();

    @Modifying
    @Query(value = "INSERT INTO Stock VALUES (:id_produit,:prix,:quantite)", nativeQuery = true)
    @Transactional
    void modifyStock(@Param("id_produit") UUID idProduit, @Param("prix") float prix,
                    @Param("quantite") int quantite);
}